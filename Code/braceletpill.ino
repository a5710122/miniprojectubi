#include <LCD5110_Graph.h>
#include <stdio.h>
#include <DS1302.h>

LCD5110 lcd(8,9,10,12,11);
extern unsigned char BigNumbers[];
extern unsigned char SmallFont[];
extern uint8_t clock[];

namespace {
  
  const int kCePin   = 5;  //RST
  const int kIoPin   = 6;  //DAT
  const int kSclkPin = 7;  //CLK
  
  
  // Create a DS1302 object.
  DS1302 rtc(kCePin, kIoPin, kSclkPin);
  Time t = rtc.time();
  
  String dayAsString(const Time::Day day) {
    switch (day) {
      case Time::kSunday: return "Sunday";
      case Time::kMonday: return "Monday";
      case Time::kTuesday: return "Tuesday";
      case Time::kWednesday: return "Wednesday";
      case Time::kThursday: return "Thursday";
      case Time::kFriday: return "Friday";
      case Time::kSaturday: return "Saturday";
    }
    return "(unknown day)";
  }
}

const int buzzer = 4;  //CLK
unsigned long taks1, taks2, taks3;

const int buttonPin = 3;
int buttonState = 0;
int hour;

void setup() {
  Serial.begin(9600);
  pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output
  pinMode(buttonPin, INPUT);
  
  setTime(); 
  lcd.InitLCD();
  lcd.clrScr();
  lcd.drawBitmap(0, 0, clock, 84, 48);
  lcd.update();  
  delay(3000);
}

void loop() {
  
  Time t = rtc.time();
  buttonState = digitalRead(buttonPin);
  
  if (millis() - taks1 >= 300) {
    taks1 = millis();
    printTime();
  }
  if (millis() - taks2 >= 500) {
    taks2 = millis();

    if(t.hr == 12 && t.min == 0 &&t.sec == 0){
      medicel();
    }else if(t.hr == 12 && t.min == 0 &&t.sec == 0){
      medicel();
    }else if((t.hr % 2) == 0 && t.min == 2 && t.sec == 0){
      activity();
    }
    
  }

        if (millis() - taks3 >= 500) {
        taks3 = millis();
        if (buttonState == HIGH) {
          Serial.print(buttonState);
        } else {
          //digitalWrite(ledPin, LOW);
          Serial.print(buttonState);
        }
      }
  
}

void medicel(){
  
  lcd.clrScr();
  lcd.setFont(SmallFont);                                                                     
  lcd.print("Time to take",0,0);
  lcd.print("the medicine",7,18);
  lcd.update();
  
  digitalWrite(buzzer, HIGH);
  delay(3000);
  digitalWrite(buzzer, LOW);
  
}

void activity(){
  
  lcd.clrScr();
  lcd.setFont(SmallFont);                                                                    
  lcd.print("Time to",0,0);
  lcd.print("activity",7,18);
  lcd.update();  
  digitalWrite(buzzer, HIGH);
  delay(2000);
  digitalWrite(buzzer, LOW);
  delay(2000);
  digitalWrite(buzzer, HIGH);
  delay(4000);
  digitalWrite(buzzer, LOW);
  
}

void printTime() {
  
  Time t = rtc.time();
  
  String month;
  String date;
  String hour;
  String minutes;
  
  month = String(t.mon);
  date = String(t.date);
  hour = String(t.hr);
  minutes = String(t.min);
  const String day = dayAsString(t.day);
  
  
  String fullDate = day+" "+month+"/"+date; 
  
  lcd.clrScr();
  lcd.setFont(SmallFont);
  lcd.print(fullDate,0,0);
  
  lcd.setFont(BigNumbers);
  
  if(t.hr<10)
  {
    hour = "0"+hour;
    lcd.print(hour,7,18);
  }else
  {
    lcd.print(hour,7,18);
  }
  lcd.print(".",35,18);
  if(t.min<10)
  {
    minutes = "0"+minutes;
    lcd.print(minutes,47,18);
    
  }else
  {
    lcd.print(minutes,47,18);
  }
  
  lcd.update();
}



void setTime()
{
  
  rtc.writeProtect(false);
  rtc.halt(false);
  Time t(2020, 3, 20, 11, 59 , 00, Time::kFriday); //Change this line to set time  ex 2015 26/2 9:09:50
  rtc.time(t);
}
